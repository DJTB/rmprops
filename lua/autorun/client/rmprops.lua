language.Add("Tool.rmprops.name", "RMProps")
language.Add("Tool.rmprops.desc", "by DJTB")
language.Add("Tool.rmprops.left", "Remove map prop")
language.Add("Tool.rmprops.right", "Add back the map prop")

if CAMI then
	CAMI.RegisterPrivilege({
		Name = "RMProps",
		MinAccess = "superadmin"
	})
end