if CAMI then
	CAMI.RegisterPrivilege({
		Name = "RMProps",
		MinAccess = "superadmin"
	})
end

local convar = CreateConVar("rmprops_enable", 1, bit.bor(FCVAR_GAMEDLL, FCVAR_LUA_SERVER), "Enable RemoveProps System (0|1)", 0, 1)

sql.Query("CREATE TABLE IF NOT EXISTS rmprops('id' INTEGER NOT NULL, 'map' TEXT NOT NULL, PRIMARY KEY('id'))")

local function removeProps()
	if not convar:GetBool() then
		print("[RMProps] RMProps is active but disabled ! Active it by typing rmprops_enable 1 !")
		return
	end

	local data = sql.Query("SELECT id FROM rmprops WHERE map = " .. sql.SQLStr(game.GetMap()))
	if not data then return end

	local entsToRemove = {}

	for i = 1, #data do
		entsToRemove[tonumber(data[i].id)] = true
	end

	local entities = ents.GetAll()

	local count = 0

	for i = 1, #entities do
		local mapID = entities[i]:MapCreationID()
		if mapID == -1 or not entsToRemove[mapID] then continue end

		entities[i]:Remove()
		count = count + 1
	end

	print(("[RMProps] Successfully removed all %d RMProps !"):format(count))
end

hook.Add("InitPostEntity", "RMProps", removeProps)
hook.Add("PostCleanupMap", "RMProps", removeProps)

cvars.AddChangeCallback("rmprops_enable", function(_, oldValue, newValue)
	if not tobool(newValue) then
		print("[RMProps] Disabled RMProps !")

		return
	end

	print("[RMProps] Enabled RMProps !")
	removeProps()
end, "RMProps")

concommand.Add("rmprops_import", function(ply)
	if IsValid(owner) and not (CAMI and CAMI.PlayerHasAccess(owner, "RMProps") or owner:IsSuperAdmin()) then return false end

	local hookTable = hook.GetTable()

	if hookTable["PostCleanupMap"]["WhenCleanUpRemoveProps"] then
		print("[RMProps] Remove Props has been detected, make sure you removed it before importing in RMProps.")
		return
	end

	local map = game.GetMap()

	print(("[RMProps] Importing Remove Props data for map %s."):format(map))
	local count = 0

	local data = sql.Query("SELECT * FROM removeprops WHERE map = " .. sql.SQLStr(map))
	if not data then return end

	local total = #data

	for i = 1, total do
		local content = util.JSONToTable(data[i].content)

		for k, v in ipairs(ents.FindInSphere(content.Pos, 0.2)) do
			if v:GetModel() == content.Model and v:GetClass() == content.Name then
				local mapID = v:MapCreationID()

				if not mapID or mapID == -1 then continue end
				sql.Query(("INSERT OR IGNORE INTO rmprops VALUES(%d, %s)"):format(mapID, sql.SQLStr(game.GetMap())))

				v:Remove()
				count = count + 1
			end
		end
	end

	print(("[RMProps] Successfully imported %d/%d props !"):format(count, total))
end)