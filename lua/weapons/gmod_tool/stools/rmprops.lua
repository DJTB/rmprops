TOOL.Name			= "RMProps"
TOOL.Category		= "Props Tool"
TOOL.Information	= {
	{name = "left"},
	{name = "right"}
}

local convar = GetConVar("rmprops_enable")

function TOOL:LeftClick(tr)
	local owner = self:GetOwner()

	if not (CAMI and CAMI.PlayerHasAccess(owner, "RMProps") or owner:IsSuperAdmin()) then return false end

	if CLIENT then return true end

	local ent = tr.Entity
	local mapID = ent:MapCreationID()

	if not IsValid(ent) or ent:IsWorld() or not mapID or mapID == -1 then
		if SERVER then
			owner:ChatPrint("You can't remove this entity.")
		end

		return false
	end

	sql.Query(("INSERT OR IGNORE INTO rmprops VALUES(%d, %s)"):format(mapID, sql.SQLStr(game.GetMap())))
	owner:ChatPrint(("You removed the map entity #%d"):format(mapID))

	if not convar:GetBool() then
		owner:ChatPrint("RMProps is disabled, see tool description to activate.")
		return true
	end

	ent:Remove()

	return true
end

function TOOL:RightClick(tr)
	local owner = self:GetOwner()

	if not (CAMI and CAMI.PlayerHasAccess(owner, "RMProps") or owner:IsSuperAdmin()) then return false end

	if CLIENT then return true end

	local ent = tr.Entity
	local mapID = ent:MapCreationID()

	if not IsValid(ent) or ent:IsWorld() or not mapID or mapID == -1 then
		if SERVER then
			owner:ChatPrint("You can't undo removal of this entity.")
		end

		return false
	end

	local removed = sql.Query(("SELECT 1 FROM rmprops WHERE id = %d AND map = %s"):format(mapID, sql.SQLStr(game.GetMap())))
	if removed and removed[1] then
		sql.Query(("DELETE FROM rmprops WHERE id = %d AND map = %s"):format(mapID, sql.SQLStr(game.GetMap())))
		owner:ChatPrint(("You added back the map entity #%d"):format(mapID))
	else
		owner:ChatPrint("This map prop isn't in the delete list")
	end

	return true
end

function TOOL:Reload()
	return false
end

function TOOL.BuildCPanel(panel)
	panel:Help("RMProps\n\nHow can I bring back a prop:\n1. rmprops_enable 0 in the server console\n2. Cleanup map\n3. Right Click on the prop\n4. rmprops_enable 1\n\nby DJTB")
end